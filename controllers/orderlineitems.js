const express = require('express')
const api = express.Router()
const Model = require('../models/orderlineitems.js')
const LOG = require('../utils/logger.js')
const find = require('lodash.find')
const remove = require('lodash.remove')
const notfoundstring = 'orderlineitems'

// RESPOND WITH JSON DATA  --------------------------------------------

// GET all JSON
api.get('/findall', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  const data = req.app.locals.orderlineitems.query
  res.send(JSON.stringify(data))
})

// GET one JSON by ID
api.get('/findone/:id', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  const id = parseInt(req.params.id, 10) // base 10
  const data = req.app.locals.orderlineitems.query
  const item = find(data, { ordLine_id: id })
  if (!item) { return res.end(notfoundstring) }
  res.send(JSON.stringify(item))
})

// RESPOND WITH VIEWS  --------------------------------------------

// GET to this controller base URI (the default)
api.get('/', (req, res) => {
  res.render('orderlineitems/index.ejs')
})

// GET create
api.get('/create', (req, res) => {
  LOG.info(`Handling GET /create${req}`)
  const item = new Model()
  LOG.debug(JSON.stringify(item))
  res.render('orderlineitems/create',
    {
      title: 'Create orderlineitems',
      layout: 'layout.ejs',
      orderline: item
    })
})

// GET /delete/:id
api.get('/delete/:id', (req, res) => {
  LOG.info(`Handling GET /delete/:id ${req}`)
  const id = parseInt(req.params.id, 10) // base 10
  const data = req.app.locals.orderlineitems.query
  const item = find(data, { ordLine_id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
  return res.render('orderlineitems/delete.ejs',
    {
      title: 'Delete orderlineitems',
      layout: 'layout.ejs',
      orderlineitems: item
    })
})

// GET /details/:id
api.get('/details/:id', (req, res) => {
  LOG.info(`Handling GET /details/:id ${req}`)
  const id = parseInt(req.params.id, 10) // base 10
  const data = req.app.locals.orderlineitems.query
  const item = find(data, { ordLine_id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
  return res.render('orderlineitems/details.ejs',
    {
      title: 'orderlineitems Details',
      layout: 'layout.ejs',
      orderlineitems: item
    })
})

// GET one
api.get('/edit/:id', (req, res) => {
  LOG.info(`Handling GET /edit/:id ${req}`)
  const id = parseInt(req.params.id, 10) // base 10
  const data = req.app.locals.orderlineitems.query
  const item = find(data, { ordLine_id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR${JSON.stringify(item)}`)
  return res.render('orderlineitems/edit.ejs',
    {
      title: 'orderlineitems',
      layout: 'layout.ejs',
      orderlineitems: item
    })
})

// HANDLE EXECUTE DATA MODIFICATION REQUESTS --------------------------------------------

// POST new
api.post('/save', (req, res) => {
  LOG.info(`Handling POST ${req}`)
  LOG.debug(JSON.stringify(req.body))
  const data = req.app.locals.orderlineitems.query
  const item = new Model()
  LOG.info(`NEW ID ${req.body.ordLine_id}`)
  item.ordLine_id = parseInt(req.body.ordLine_id, 10) // base 10
  item.order_id = req.body.order_id
  item.product_id = req.body.product_id
  item.order_Quantity = req.body.order_Quantity
    data.push(item)
    LOG.info(`SAVING NEW orderlineitems ${JSON.stringify(item)}`)
    return res.redirect('/orderlineitems')
  
})

// POST update
api.post('/save/:id', (req, res) => {
  LOG.info(`Handling SAVE request ${req}`)
  const id = parseInt(req.params.id, 10) // base 10
  LOG.info(`Handling SAVING ID=${id}`)
  const data = req.app.locals.orderlineitems.query
  const item = find(data, { ordLine_id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`ORIGINAL VALUES ${JSON.stringify(item)}`)
  LOG.info(`UPDATED VALUES: ${JSON.stringify(req.body)}`)
  item.order_id = req.body.order_id
  item.product_id = req.body.product_id
  item.order_Quantity = req.body.order_Quantity
    LOG.info(`SAVING UPDATED orderlineitems ${JSON.stringify(item)}`)
    return res.redirect('/orderlineitems')
  
})

// DELETE id (uses HTML5 form method POST)
api.post('/delete/:id', (req, res) => {
  LOG.info(`Handling DELETE request ${req}`)
  const id = parseInt(req.params.id, 10) // base 10
  LOG.info(`Handling REMOVING ID=${id}`)
  const data = req.app.locals.orderlineitems.query
  const item = find(data, { ordLine_id: id })
  if (!item) {
    return res.end(notfoundstring)
  }
  if (item.isActive) {
    item.isActive = false
    console.log(`Deacctivated item ${JSON.stringify(item)}`)
  } else {
    const item = remove(data, { ordLine_id: id })
    console.log(`Permanently deleted item ${JSON.stringify(item)}`)
  }
  return res.redirect('/orderlineitems')
})

module.exports = api
