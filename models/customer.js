/** 
*  Customer model
*  Describes the characteristics of each attribute in a customer resource.
*
* @author Sanjana Tummala<sanjanatummala4466@gmail.com>
*
*/

// bring in mongoose 
// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const CustomerSchema = new mongoose.Schema({

  _id: { type: Number, required: true },
  email: {
    type: String,
    required: true,
    unique: true
  },
  Firstname: {
    type: String,
    required: true,
    default: 'Please type firstName'
  },
  Lastname: {
    type: String,
    required: true,
    default: 'Please type lastName'
  },
  street1: {
    type: String,
    required: true,
    default: 'Please type Street 1'
  },
  street2: {
    type: String,
    required: false,
    default: 'Please type Street 2'
  },
  city: {
    type: String,
    required: true,
    default: 'Please type city'
  },
  state: {
    type: String,
    required: true,
    default: 'Please type city'
  },
  zip: {
    type: String,
    required: true,
    default: '00000'
  },
  country: {
    type: String,
    required: true,
    default: 'International'
  }
  


})
module.exports = mongoose.model('Customer', CustomerSchema)