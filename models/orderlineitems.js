//SatyaKanth Kolakani
const mongoose = require('mongoose')

const orderLineItemsSchema = new mongoose.Schema({

  ordLine_id: { type: Number, required: true },
  order_id: {
    type: Number,
    required: true,
    default: 100
  },
  product_id: {
    type: Number,
    required: true,
    default: 0
  },
  order_Quantity: {
    type: Number,
    required: false,
    default: 0
  }
})
module.exports = mongoose.model('orderLineItems', orderLineItemsSchema)