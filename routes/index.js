/**
 * @index.js - manages all routing
 *
 * router.get when assigning to a single request
 * router.use when deferring to a controller
 *
 * @requires express
 */

const express = require('express')
const LOG = require('../utils/logger.js')

LOG.debug('START routing')
const router = express.Router()

// Manage top-level request first
router.get('/', (req, res, next) => {
  LOG.debug('Request to /')
  res.render('index.ejs', { title: 'Express App' })
})
router.get("/products", function (req, res) {
  res.render("products/index.ejs")
 });
 router.get("/order", function (req, res) {
  res.render("order/index.ejs")
 });
 router.get("/orderlineitems", function (req, res) {
  res.render("orderlineitems/index.ejs")
 });
 router.get("/customer", function (req, res) {
  res.render("customer/index.ejs")
 });
 router.get("/about", function (req, res) {

  res.render("about/index.ejs")
 });



// Defer path requests to a particular controller
router.use('/order', require('../controllers/order.js')) 
router.use('/customer', require('../controllers/customer.js'))
router.use('/orderlineitems', require('../controllers/orderlineitems.js'))
router.use('/products', require('../controllers/product.js'))


LOG.debug('END routing')
module.exports = router